#!/bin/bash -e
PATH=/bin:/sbin:/usr/bin:/usr/sbin:/snap/bin
LXCUSER="root"

if [ "$4" == "" ]; then
	echo "usage: migrate.sh <srcdir> <machine name> <machine hwaddr> <machine ip>"
	exit 1
fi
SRC="$1"
NAME="$2"
HWADDR="$3"
IP="$4"
DST="/var/lib/lxc/$NAME/rootfs"

UID_START=$(grep "$LXCUSER": /etc/subuid |cut -d ':' -f 2)
GID_START=$(grep "$LXCUSER": /etc/subgid |cut -d ':' -f 2)


mkdir /var/lib/lxc/$NAME
echo "Generating config from template..."
cat config |sed "s/@MACHINE_NAME@/$NAME/g;s/@MACHINE_HWADDR@/$HWADDR/g;s/@UID_START@/$UID_START/g;s/@GID_START@/$GID_START/g" > /var/lib/lxc/$NAME/config


echo "Migration source: $SRC"
echo "Migration dest: $DST"

echo "Container UID start: $UID_START"
echo "Container GID start: $GID_START"

UIDS=$(cat $SRC/etc/passwd |cut -d ':' -f 3 |xargs echo)
GIDS=$(cat $SRC/etc/group |cut -d ':' -f 3 |xargs echo)
echo "List of UIDs to convert: $UIDS"
echo "List of GIDs to convert: $GIDS"

UIDMAP=""
for U in $UIDS ; do
  if [ "$UIDMAP" != "" ]; then
    UIDMAP="$UIDMAP,"
  fi
  UIDMAP="$UIDMAP$U:$(($U + $UID_START))"
done
echo "UID mapping: $UIDMAP"

GIDMAP=""
for U in $GIDS ; do
  if [ "$GIDMAP" != "" ]; then
    GIDMAP="$GIDMAP,"
  fi
  GIDMAP="$GIDMAP$U:$(($U + $GID_START))"
done
echo "GID mapping: $GIDMAP"

echo "Copying..."
rsync -aHSAXx --numeric-ids --usermap $UIDMAP --groupmap $GIDMAP $SRC $DST

echo "Generating /etc/network/interfaces from template ..."
cat interfaces |sed "s/@MACHINE_IP@/$IP/g" > /var/lib/lxc/$NAME/rootfs/etc/network/interfaces
